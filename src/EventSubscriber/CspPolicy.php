<?php

namespace Drupal\csp_google\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Drupal\csp_google\GoogleSupportedDomainsHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alter the CSP policy to add Google's domains where needed.
 */
class CspPolicy implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Google supported domains helper.
   *
   * @var \Drupal\csp_google\GoogleSupportedDomainsHelper
   */
  protected $googleSupportedDomainsHelper;

  /**
   * Constructs a new \Drupal\csp_google\EventSubscriber\CspPolicy object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\csp_google\GoogleSupportedDomainsHelper $googleSupportedDomainsHelper
   *   Google supported domains helper.
   */
  public function __construct(ConfigFactoryInterface $configFactory, GoogleSupportedDomainsHelper $googleSupportedDomainsHelper) {
    $this->configFactory = $configFactory;
    $this->googleSupportedDomainsHelper = $googleSupportedDomainsHelper;
  }

  /**
   * Alter CSP policy.
   *
   * @param \Drupal\csp\Event\PolicyAlterEvent $alterEvent
   *   The Policy Alter event.
   */
  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent) {
    $policy = $alterEvent->getPolicy();

    $policy_type_key = $policy->isReportOnly() ? 'report-only' : 'enforce';
    $policy_type_key_config = $this->configFactory->get('csp.settings')
      ->get($policy_type_key);

    static $supported_domains = NULL;

    foreach ($policy_type_key_config['directives'] as $key => $value) {
      if (!$policy->hasDirective($key)) {
        continue;
      }
      if (!is_array($value) || !$value['csp_google_add_google_domain_sources']) {
        continue;
      }

      if ($supported_domains === NULL) {
        $supported_domains = $this->googleSupportedDomainsHelper->getSupportedDomains();
      }

      $directive = $policy->getDirective($key);
      foreach ($supported_domains as $supported_domain) {
        $source = '*.' . $supported_domain;
        if (!in_array($source, $directive)) {
          $directive[] = $source;
          $directive[] = $supported_domain;
        }
        if (!in_array($supported_domain, $directive)) {
          $directive[] = $supported_domain;
        }
      }
      $policy->setDirective($key, $directive);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[CspEvents::POLICY_ALTER] = ['onCspPolicyAlter'];
    return $events;
  }

}
