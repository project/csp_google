<?php

namespace Drupal\csp_google;

use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client;

/**
 * Helper functions to determine Google's domain names.
 */
class GoogleSupportedDomainsHelper {

  const URL = 'https://www.google.com/supported_domains';

  const STATE_KEY = 'csp_google_supported_domains';

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new \Drupal\csp_google\GoogleSupportedDomainsHelper object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   */
  public function __construct(StateInterface $state, Client $httpClient) {
    $this->state = $state;
    $this->httpClient = $httpClient;
  }

  /**
   * Update the list of supported domains.
   */
  public function updateSupportedDomains(): void {
    $response = $this->httpClient->get(self::URL);
    $response_content = $response->getBody()->getContents();

    if ($response->getStatusCode() != 200 || empty($response_content)) {
      throw new \RuntimeException('Could not update supported domains from ' . self::URL);
    }

    $supported_domains = [];

    foreach (explode("\n", $response_content) as $domain) {
      $domain = trim($domain, " \t\n\r\0\x0B.");
      if (empty($domain)) {
        continue;
      }
      $supported_domains[] = trim($domain);
    }

    if (empty($supported_domains)) {
      throw new \RuntimeException('Could not update supported domains from ' . self::URL);
    }

    $this->state->set(self::STATE_KEY, $supported_domains);
  }

  /**
   * Get the list of supported domains.
   *
   * @return string[]
   *   The domains.
   */
  public function getSupportedDomains(): array {
    $supported_domains = $this->state->get(self::STATE_KEY);
    if (!is_array($supported_domains)) {
      $this->updateSupportedDomains();
      $supported_domains = $this->state->get(self::STATE_KEY);

      if (!is_array($supported_domains)) {
        throw new \RuntimeException('List of supported domains is empty.');
      }
    }

    return $supported_domains;
  }

}
