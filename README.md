# CSP Google Supported Domains

This modules automatically adds the domain names listed on
https://www.google.com/supported_domains to the CSP policy created by the CSP
module.

After installation, the domain names can be added by checking the
'Add Google supported domains' checkbox when editing the CSP policy.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/csp_google).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/csp_google).

# Why should I use this?
This is only required if you're using google features that load assets through
country-code top level domains. Basic analytics functionality does not require
these domains, but some of their ad-related features do.

# Content-Security-Policy header length
Please note that when 'Add Google supported domains' is enabled for (all)
individual CSP policies, you might run into issues caused by the length of
the Content-Security-Policy header. When the length is larger than what your
reverse proxy or CDN supports, it might show an 502 error.

Please see https://www.drupal.org/project/drupal/issues/2844620 and
https://www.drupal.org/project/drupal/issues/2954339 for more information.